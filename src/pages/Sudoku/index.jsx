import React, { Component } from 'react';

import Game from './components/Game';
import Settings from './components/Settings';

class Sudoku extends Component {
  state = {
    settings: {},
    isConfigured: false,
  };

  getSettings = (settings) => {
    this.setState({
      settings,
      isConfigured: true,
    });
  }

  endGame = () => {
    this.setState({
      settings: {},
      isConfigured: false,
    });
  }

  render() {
    if (this.state.isConfigured) {
      return (
        <Game
          settings={this.state.settings}
          endGame={this.endGame}
        />
      );
    }

    return <Settings getSettings={this.getSettings} />;
  }
}

export default Sudoku;
