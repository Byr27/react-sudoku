import React, { Component } from 'react';
import './style.scss';

// Material
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';

// Data
import sudokuFields from '../../sudoku';

class Settings extends Component {
  state = {
    settings: {
      mode: '',
      level: '',
      levelIndex: '',
      sudokuIndex: '',
    },
  }

  handleChange = ({ target }) => {
    const stateCopy = Object.assign({}, this.state);
    stateCopy.settings[target.name] = target.value;

    this.setState(stateCopy);
  }

  render() {
    return (
      <div className="g-container settings">
        <div className="settings__wrapper">

          <h1 className="settings__section settings__section--center">
            Выберите режим!
          </h1>

          <div className="settings__section settings__section--around">
            <RadioGroup
              aria-label="gender"
              name="mode"
              value={this.state.settings.mode}
              onChange={this.handleChange}
            >
              <FormControlLabel
                value="filled"
                control={<Radio color="primary" />}
                label="Заготовленные уровни"
              />
              <FormControlLabel
                value="empty"
                control={<Radio color="primary" />}
                label="Пустые поля"
              />
            </RadioGroup>
          </div>

          <div className="settings__section settings__section--around">
            {this.state.settings.mode === 'empty'
              ? (
                <FormControl className="settings__select">
                  <InputLabel>Размер</InputLabel>
                  <Select
                    name="level"
                    value={this.state.settings.level}
                    onChange={this.handleChange}
                  >
                    <MenuItem value={2}>4x4</MenuItem>
                    <MenuItem value={3}>9x9</MenuItem>
                  </Select>
                </FormControl>
              )
              : null
            }
            {this.state.settings.mode === 'filled'
              ? (
                <div className="settings__select-group">
                  <div className="settings__select-wrapper">
                    <FormControl className="settings__select">
                      <InputLabel>Размер</InputLabel>
                      <Select
                        name="levelIndex"
                        value={this.state.settings.levelIndex}
                        onChange={this.handleChange}
                      >
                        {sudokuFields.sudoku.map((level, levelIndex) => (
                          <MenuItem
                            key={level.id}
                            value={levelIndex}
                          >
                            {level.name}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>
                  {(this.state.settings.levelIndex > 0 || this.state.settings.levelIndex === 0)
                    ? (
                      <div className="settings__select-wrapper">
                          <FormControl className="settings__select">
                            <InputLabel>Уровень</InputLabel>
                            <Select
                              name="sudokuIndex"
                              value={this.state.settings.sudokuIndex}
                              onChange={this.handleChange}
                            >
                              {sudokuFields.sudoku[this.state.settings.levelIndex].puzzles
                                .map((sudoku, sudokuIndex) => (
                                  <MenuItem
                                    key={sudoku.id}
                                    value={sudokuIndex}
                                  >
                                    уровень {sudoku.id}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>
                      </div>
                    )
                    : null
                  }
                </div>
              )
              : null
            }
          </div>

          <div className="settings__section settings__section--center">
            {((this.state.settings.mode === 'empty'
              && this.state.settings.level)
              || (this.state.settings.mode === 'filled'
              && (this.state.settings.levelIndex === 0 || this.state.settings.levelIndex > 0)
              && (this.state.settings.sudokuIndex === 0 || this.state.settings.sudokuIndex > 0)))
              && <Button
              variant="contained"
              color="primary"
              onClick={() => this.props.getSettings(this.state.settings)}
            >
              Запуск
            </Button>}
          </div>
        </div>
      </div>
    );
  }
}

export default Settings;
