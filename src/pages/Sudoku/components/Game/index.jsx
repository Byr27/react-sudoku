import React, { Component } from 'react';
import './style.scss';

// Components
import Modal from '@components/Modal';

// Material
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

// libs
import { clone } from 'chober';

// Data
import sudokuFields from '../../sudoku';

class Game extends Component {
  state = {
    level: null,
    cells: [],
    fixedCells: [],
    numberKeyboard: [],
    selectedCell: '',
    isEndGame: false,
    isVictory: false,
    isFixedFields: false,
  }

  componentDidMount() {
    this.setLevel();
  }

  // Build cells
  setLevel = () => {
    if (this.props.settings.mode === 'empty') {
      this.setState({ level: this.props.settings.level });
    }

    if (this.props.settings.mode === 'filled') {
      this.setState({ level: sudokuFields.sudoku[this.props.settings.levelIndex].level });
    }

    setTimeout(() => {
      this.setCells();
      this.setNumberKeyboard();
    }, 0);
  }

  setCells = () => {
    if (this.props.settings.mode === 'empty') {
      const data = [];

      for (let row = 0; row < this.state.level ** 2; row += 1) {
        data[row] = [];
        for (let cell = 0; cell < this.state.level ** 2; cell += 1) {
          data[row][cell] = '';
        }
      }

      this.setState({ cells: data });
      this.setStyle();
    }

    if (this.props.settings.mode === 'filled') {
      this.setState({
        cells: clone(sudokuFields
          .sudoku[this.props.settings.levelIndex]
          .puzzles[this.props.settings.sudokuIndex]
          .cells),
      });
      this.setStyle();
      this.fixCells();
    }
  }

  setNumberKeyboard = () => {
    const numberKeyboard = [];

    for (let number = 1; number <= this.state.level ** 2; number += 1) {
      numberKeyboard[number - 1] = number;
    }

    this.setState({ numberKeyboard });
  }

  // Style
  cellStyle = () => {
    const style = {};
    let sideSize;

    if (this.state.level === 2) {
      sideSize = 100;
      style.width = sideSize;
      style.height = sideSize;
      style.fontSize = sideSize * 0.5;
    }

    if (document.documentElement.clientWidth < 500) {
      sideSize = ((document.documentElement.clientWidth - 20) / (this.state.level ** 2));
      style.width = sideSize;
      style.height = sideSize;
      style.fontSize = sideSize * 0.5;
    }

    return style;
  }

  setStyle = () => {
    const arrFromInput = document.querySelectorAll('.game__cell');
    let step = true;
    const { level } = this.state;

    for (let i = 0; i < level ** 4; i += 1) {
      if (level % 2) {
        if (i % (level ** 3) === 0) {
          step = !step;
        }
        if (i % (level ** 2) === 0) {
          step = !step;
        }
        if (i % level === 0) {
          step = !step;
        }
      } else {
        if (i % (level ** 3) === 0) {
          step = !step;
        }
        if (i % level === 0) {
          step = !step;
        }
      }

      arrFromInput[i].classList.toggle('game__cell--dark', step);
    }
  }

  // Fix/clear
  fixCells = () => {
    const arrFromInput = document.querySelectorAll('.game__cell');

    arrFromInput.forEach((input) => {
      if (input.innerHTML) {
        input.classList.add('game__cell--readonly');
        this.setState({ isFixedFields: true });
      }
    });
    if (this.state.selectedCell !== '') {
      this[this.state.selectedCell].focus();
    }
    this.setState({ fixedCells: clone(this.state.cells) });
  }

  clearFields = () => {
    const arrFromInput = document.querySelectorAll('.game__cell');

    arrFromInput.forEach((input) => {
      if (!input.classList.contains('game__cell--readonly')) {
        input.innerHTML = '';
        input.classList.remove('game__cell--filled');
      }
    });
    if (this.state.selectedCell !== '') {
      this[this.state.selectedCell].focus();
    }
    this.setState({ cells: clone(this.state.fixedCells) });
  }

  // Game
  setSelectedCell = (rowIndex, cellIndex) => {
    const selectedCell = `c${rowIndex}${cellIndex}`;
    const arrFromCells = document.querySelectorAll('.game__cell');
    arrFromCells.forEach((cell) => {
      cell.classList.remove('game__cell--focus');
    });
    this.setState({ selectedCell });
    this[selectedCell].classList.add('game__cell--focus');
  }

  moveFocus = ({ key }) => {
    const { selectedCell } = this.state;
    if (selectedCell === '') return;
    const row = Number(selectedCell.charAt(1));
    const column = Number(selectedCell.charAt(2));
    let nextCell;

    switch (key) {
      case 'ArrowUp':
        if (row - 1 < 0) return;

        nextCell = `c${row - 1}${column}`;
        break;
      case 'ArrowDown':
        if (row + 1 > (this.state.level ** 2) - 1) return;
        nextCell = `c${row + 1}${column}`;
        break;
      case 'ArrowLeft':
        if (column - 1 < 0) return;

        nextCell = `c${row}${column - 1}`;
        break;
      case 'ArrowRight':
        if (column + 1 > (this.state.level ** 2) - 1) return;

        nextCell = `c${row}${column + 1}`;
        break;
      default:
        return;
    }

    const arrFromCells = document.querySelectorAll('.game__cell');

    arrFromCells.forEach((cell) => {
      cell.classList.remove('game__cell--focus');
    });

    this.setState({ selectedCell: nextCell });
    this[nextCell].focus();
    this[nextCell].classList.add('game__cell--focus');
  }

  updateCellfromInput = (event) => {
    const { selectedCell } = this.state;
    const rowIndex = Number(selectedCell.charAt(1));
    const cellIndex = Number(selectedCell.charAt(2));

    if (!event.target.classList.contains('game__cell--readonly')) {
      const stateCopy = Object.assign({}, this.state);
      if (event.key === 'Backspace') {
        this[selectedCell].innerHTML = '';
        this[selectedCell].classList.remove('game__cell--filled');
      }

      if (event.key > 0 && event.key <= this.state.level ** 2) {
        this[selectedCell].innerHTML = event.key;
        this[selectedCell].classList.add('game__cell--filled');
      }

      stateCopy.cells[rowIndex][cellIndex] = event.target.innerHTML;

      this.setState(stateCopy);
    }
    this.check();
  }

  updateCellfromClick = ({ target }) => {
    const { selectedCell } = this.state;
    if (selectedCell !== '' && !this[selectedCell].classList.contains('game__cell--readonly')) {
      const firstNumber = selectedCell.charAt(1);
      const secondNumber = selectedCell.charAt(2);
      const stateCopy = clone(this.state);

      this[selectedCell].innerHTML = target.getAttribute('value');

      if (target.getAttribute('value')) {
        this[selectedCell].classList.add('game__cell--filled');
      } else {
        this[selectedCell].classList.remove('game__cell--filled');
      }

      stateCopy.cells[firstNumber][secondNumber] = target.getAttribute('value');

      this.setState(stateCopy);


      setTimeout(() => this.check(), 0);
      this[selectedCell].focus();
    }
  }

  check = () => {
    const { level } = this.state;
    const example = [];

    for (let number = 1; number <= level ** 2; number += 1) {
      example[number - 1] = number;
    }

    const rCells = this.state.cells.reduce((isCells, row, rowIndex, cells) => {
      let isRight = isCells;

      const checkHorizontal = row.map((cell, cellIndex) => cells[rowIndex][cellIndex]);
      const checkVertical = row.map((cell, cellIndex) => cells[cellIndex][rowIndex]);
      const checkSquare = [];
      const subCheckSquare = [];

      for (let squares = 0; squares < level; squares += 1) {
        for (let square = 0; square < level; square += 1) {
          for (let line = 0; line < level; line += 1) {
            for (let column = 0; column < level; column += 1) {
              checkSquare
                .push(cells[(line + 1 + (level * (squares))) - 1][(column) + level * (square)]);
            }
          }
        }
      }

      for (let i = 0; i < level ** 2; i += 1) {
        subCheckSquare[i] = checkSquare.slice(i * (level ** 2), ((level ** 2) * (i + 1)));
      }

      subCheckSquare.forEach((checking) => {
        checking.sort().forEach((number, numberIndex) => {
          if (+number !== example[numberIndex]) {
            isRight *= 0;
          }
        });
      });

      [checkVertical, checkHorizontal].forEach((checking) => {
        checking.sort().forEach((number, numberIndex) => {
          if (+number !== example[numberIndex]) {
            isRight *= 0;
          }
        });
      });

      return isRight;
    }, 1);

    if (rCells) this.setState({ isVictory: true });
  }

  render() {
    return (
      <div
        className="g-container"
        onKeyDown={this.moveFocus}
      >
        <div className="game__cells">
          <div>
            {this.state.cells.map((row, rowIndex) => (
              <div
                key={rowIndex}
                className="game__row"
              >
                {row.map((cell, cellIndex) => (
                  <div
                    style={ this.cellStyle() }
                    key={cellIndex}
                    ref={(node) => { this[`c${rowIndex}${cellIndex}`] = node; }}
                    onKeyDown={this.updateCellfromInput}
                    onClick={() => this.setSelectedCell(rowIndex, cellIndex)}
                    onFocus={() => this.setSelectedCell(rowIndex, cellIndex)}
                    className="game__cell"
                    maxLength={this.state.level <= 3 ? 1 : 2}
                    tabIndex="0"
                  >
                    {cell}
                  </div>
                ))}
              </div>
            ))}
          </div>
        </div>
        <div>
          <div className="game__keyboard">
            {this.state.numberKeyboard.map(number => (
              <div
                value={number}
                className="game__keyboard-button"
                style={ this.cellStyle() }
                onClick={this.updateCellfromClick}
                key={number}
              >
                {number}
              </div>
            ))}
            <div
              className="game__keyboard-button"
              value=""
              style={ this.cellStyle() }
              onClick={this.updateCellfromClick}
            >
              ✖
            </div>
          </div>
        </div>
        <div className="game__settings">
          <div className="game__button">
            <Button
              variant="contained"
              color="primary"
              size={document.documentElement.clientWidth < 430
                ? 'small'
                : 'medium'}
              onClick={() => this.setState({ isEndGame: true })}
            >
              Закончить игру
            </Button>
          </div>
          <div className="game__button">
            <Button
              variant="contained"
              color="primary"
              size={document.documentElement.clientWidth < 430
                ? 'small'
                : 'medium'}
              onClick={(!this.state.isFixedFields
                && this.props.settings.mode === 'empty')
                ? this.fixCells
                : this.clearFields}
            >
              {(!this.state.isFixedFields
                && this.props.settings.mode === 'empty')
                ? 'Зафиксировать ячейки'
                : 'Очистить ячейки'}
            </Button>
          </div>
        </div>
        {this.state.isEndGame
          ? <Modal
          slot={
            <div>
              <Typography variant="h6" id="modal-title">
                Вы уверены что хотите завершить?
              </Typography>
              <div className="game__modal-footer">
                <div className="game__material-button-wrapper">
                  <Button
                    onClick={() => this.setState({ isEndGame: false })}
                    size={document.documentElement.clientWidth < 430
                      ? 'small'
                      : 'medium'}
                  >
                    Отмена
                  </Button>
                </div>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.props.endGame}
                  size={document.documentElement.clientWidth < 430
                    ? 'small'
                    : 'medium'}
                >
                  Завершить
                </Button>
              </div>
            </div>
          }
          close={() => this.setState({ isEndGame: false })}
          />
          : null}
        {this.state.isVictory
          ? (
            <Modal
              slot={
                <div>
                  <Typography variant="h6" id="modal-title">
                    Вы победили
                  </Typography>
                  <div className="game__modal-footer">
                    <div className="game__material-button-wrapper">
                      <Button
                        onClick={() => this.setState({ isVictory: false })}
                      >
                        Продолжить
                      </Button>
                    </div>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.props.endGame}
                    >
                      Выбрать другой уровень
                    </Button>
                  </div>
                </div>
              }
              close={() => this.setState({ isVictory: false })}
            />
          )
          : null}
      </div>
    );
  }
}

export default Game;
