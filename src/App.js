// React
import React from 'react';
import ReactDOM from 'react-dom';

// Styles
import '@global/scss/global.scss';
import 'normalize.css';

// Page
import Sudoku from '@pages/Sudoku';

const App = () => <Sudoku />;

ReactDOM.render(<App/>, document.getElementById('root'));
