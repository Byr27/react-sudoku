import React from 'react';
import './style.scss';

const Modal = props => (
  <div className="modal">
    <div
      className="modal__overlay"
      onClick={() => { props.close(); }}
    >
      <button
        onClick={() => { props.close(); }}
        type="button"
        className="modal__close"
      >
        ×
      </button>
    </div>
    <div className="modal__container">
      { props.slot }
    </div>
  </div>
);

export default Modal;
